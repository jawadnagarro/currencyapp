package com.example.currencyapplication.utils

class CurrencyUtil {
    /**
     * Convert currency rate equivalent to 1 EURO
     */
    fun convertCurrencyRate(
        baseCurrencyRate: Double,
        conversionCurrencyRate: Double
    ): Double {
        return try {
            ((1 / baseCurrencyRate) * conversionCurrencyRate).toDouble()
        } catch (e: Exception) {
            0.0
        }
    }
}