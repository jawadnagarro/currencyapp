package com.example.currencyapplication.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

class DateUtil {
    companion object {
        const val DEFAULT_FORMAT_DATE = "yyyy-MM-dd"
    }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentDate(date: Date = Date(), formatStr: String = DEFAULT_FORMAT_DATE): String {
        return SimpleDateFormat(formatStr).format(
            date
        )
    }

    fun getPreviousDate(previousDateCount: Int): String {
        val dateFormat = SimpleDateFormat(DEFAULT_FORMAT_DATE, Locale.getDefault())
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.add(Calendar.DATE, previousDateCount)
        return dateFormat.format(calendar.time)
    }
}