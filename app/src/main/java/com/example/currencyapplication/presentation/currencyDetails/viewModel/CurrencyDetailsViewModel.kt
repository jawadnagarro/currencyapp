package com.example.currencyapplication.presentation.currencyDetails.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.currencyapplication.data.remote.helper.APIResult
import com.example.currencyapplication.domain.model.HistoricalCurrencyModel
import com.example.currencyapplication.domain.usecase.HistoricalCurrencyUseCase
import com.example.currencyapplication.presentation.base.BaseViewModel
import com.example.currencyapplication.presentation.currencyDetails.adapter.ItemViewModel
import com.example.currencyapplication.presentation.currencyDetails.itemviewmodels.CurrencyListingViewModel
import com.example.currencyapplication.presentation.currencyDetails.itemviewmodels.HeaderViewModel
import com.example.currencyapplication.utils.CurrencyUtil
import com.example.currencyapplication.utils.DateUtil
import com.example.currencyapplication.utils.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CurrencyDetailsViewModel @Inject constructor(private val historicalCurrencyUseCase: HistoricalCurrencyUseCase) :
    BaseViewModel() {

    private var _progressStatus: MutableLiveData<Boolean> = MutableLiveData()
    val progressStatus: LiveData<Boolean> get() = _progressStatus

    private var _apiMessage: MutableLiveData<Event<String>> = MutableLiveData()
    val apiMessage: LiveData<Event<String>> get() = _apiMessage

    val historicalCurrencyData: LiveData<List<ItemViewModel>> get() = _historicalCurrencyData
    private val _historicalCurrencyData = MutableLiveData<List<ItemViewModel>>(emptyList())

    val otherCurrencyData: LiveData<String> get() = _otherCurrencyData
    private val _otherCurrencyData = MutableLiveData<String>()

    var currencyList: List<String> = emptyList()
    lateinit var baseCurrency: String
    lateinit var convertToCurrency: String

    /**
     * Set's the value from the home screen
     */
    fun setAvailableCurrencyList(
        currencyListData: Array<String>,
        baseCurrencyValue: String,
        convertToCurrencyValue: String
    ) {
        if (currencyList.isEmpty()) {
            baseCurrency = baseCurrencyValue
            convertToCurrency = convertToCurrencyValue
            currencyList = currencyListData.toList()
            getHistoricalCurrencyDate()
        }
    }

    /**
     * Get's historical currency data
     */
    private fun getHistoricalCurrencyDate() {
        historicalCurrencyUseCase.getHistoricalCurrencyData(
            currencyList,
            viewModelScope
        ) {
            when (it.status) {
                APIResult.Status.SUCCESS -> {
                    _progressStatus.value = false
                    it.data?.let { historicalCurrencyModelMap ->
                        val viewData = createViewData(historicalCurrencyModelMap)
                        _historicalCurrencyData.postValue(viewData)
                        historicalCurrencyModelMap[DateUtil().getCurrentDate()]?.let { historicalCurrency ->
                            _otherCurrencyData.value =
                                getOtherCurrencyList(historicalCurrency.rates, localCurrencyList)
                        }
                    }
                }
                APIResult.Status.ERROR -> {
                    _progressStatus.value = false
                    _apiMessage.value = Event(it.message!!)
                }
                APIResult.Status.LOADING -> _progressStatus.value = true
            }
        }
    }

    /**
     * Get currency rate for other currency
     */
    fun getOtherCurrencyList(
        currencyRateMap: Map<String, Double>,
        localCurrencyList: List<String>
    ): String {
        val otherCurrency = StringBuilder()
        localCurrencyList.forEach { convertToCurrencyData ->
            val baseCurrencyRate = currencyRateMap[baseCurrency]
            val convertToCurrencyRate = currencyRateMap[convertToCurrencyData]

            if (baseCurrencyRate != null && convertToCurrencyRate != null) {
                val currencyRate = CurrencyUtil().convertCurrencyRate(
                    baseCurrencyRate,
                    convertToCurrencyRate
                )
                otherCurrency.append(
                    "1 $baseCurrency = ${
                        String.format(
                            "%.3f",
                            currencyRate
                        )
                    } $convertToCurrencyData"
                ).append("\n\n")
            }
        }
        return otherCurrency.toString()
    }

    private fun createViewData(historicalCurrencyMap: Map<String, HistoricalCurrencyModel>): List<ItemViewModel> {
        val viewData = mutableListOf<ItemViewModel>()
        historicalCurrencyMap.keys.forEach { historicalCurrency ->
            viewData.add(HeaderViewModel(historicalCurrency))
            historicalCurrencyMap[historicalCurrency]?.let { currency ->
                val baseCurrencyRate = currency.rates[baseCurrency]
                val convertToCurrencyRate = currency.rates[convertToCurrency]

                val currencyRate = CurrencyUtil().convertCurrencyRate(
                    baseCurrencyRate!!,
                    convertToCurrencyRate!!
                )
                val item =
                    CurrencyListingViewModel(
                        "1 $baseCurrency", "${
                            String.format(
                                "%.3f",
                                currencyRate
                            )
                        } $convertToCurrency"
                    )
                viewData.add(item)
            }
        }
        return viewData
    }

    companion object {
        const val DATE_ITEM = 0
        const val LISTING_ITEM = 1

        val localCurrencyList = listOf(
            "CAD",
            "GBP",
            "AED",
            "JPY",
            "SAR",
            "EGP",
            "KWD",
            "AUD",
            "KRW",
            "USD"
        )
    }
}