package com.example.currencyapplication.presentation.currencyDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.currencyapplication.databinding.FragmentCurrencyDetailsBinding
import com.example.currencyapplication.presentation.base.BaseFragment
import com.example.currencyapplication.presentation.currencyDetails.viewModel.CurrencyDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CurrencyDetailsFragment : BaseFragment() {

    private val currencyDetailsViewModel: CurrencyDetailsViewModel by viewModels()
    lateinit var binding: FragmentCurrencyDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCurrencyDetailsBinding.inflate(layoutInflater).apply {
            currencyDetails = currencyDetailsViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        arguments?.let {
            val currencyList = CurrencyDetailsFragmentArgs.fromBundle(it).currencyList
            val baseCurrency = CurrencyDetailsFragmentArgs.fromBundle(it).baseCurrency
            val convertToCurrency = CurrencyDetailsFragmentArgs.fromBundle(it).convertToCurrency
            currencyDetailsViewModel.setAvailableCurrencyList(currencyList, baseCurrency, convertToCurrency)
        }
        subscribeUI()
        return binding.root
    }

    /**
     * Observer all list to update UI on data change. If MutableLiveData already has data
     * set, it will be delivered to the observer.
     * When data changes views will receive the last available data from the server and
     * local database.
     */
    private fun subscribeUI() {
        currencyDetailsViewModel.apiMessage.observe(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.let { message ->
                showSnackBar(message)
            }
        }
    }
}