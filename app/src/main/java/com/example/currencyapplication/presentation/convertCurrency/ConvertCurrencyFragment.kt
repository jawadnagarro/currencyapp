package com.example.currencyapplication.presentation.convertCurrency

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.currencyapplication.databinding.FragmentConvertCurrencyBinding
import com.example.currencyapplication.presentation.base.BaseFragment
import com.example.currencyapplication.presentation.convertCurrency.viewModel.ConvertCurrencyViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ConvertCurrencyFragment : BaseFragment() {

    private lateinit var binding: FragmentConvertCurrencyBinding
    private val currencyViewModel: ConvertCurrencyViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentConvertCurrencyBinding.inflate(layoutInflater).apply {
            convertCurrencyViewModel = currencyViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        val rootView = binding.root
        setSpinnerListener()
        setTextWatcherListener()
        binding.btSwap.setOnClickListener {
            currencyViewModel.swapCurrencyValue()
        }
        subscribeUI()
        return rootView
    }

    /**
     * Observer all list to update UI on data change. If MutableLiveData already has data
     * set, it will be delivered to the observer.
     * When data changes views will receive the last available data from the server and
     * local database.
     */
    private fun subscribeUI() {
        currencyViewModel.convertedToCurrency.observe(viewLifecycleOwner) {
            binding.edConvertedCurrency.setText(String.format("%.6f", it))
        }
        currencyViewModel.apiMessage.observe(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.let { message ->
                showSnackBar(message)
            }
        }
        currencyViewModel.currencyList.observe(viewLifecycleOwner) { currencyList ->
            binding.btDetails.setOnClickListener {
                val baseCurrency = binding.spBaseCurrency.selectedItem.toString()
                val convertToCurrency = binding.spConvertedCurrency.selectedItem.toString()
                val action =
                    ConvertCurrencyFragmentDirections.actionConvertCurrencyFragmentToCurrencyDetailsFragment(
                        currencyList.toTypedArray(), baseCurrency, convertToCurrency
                    )
                findNavController().navigate(action)
            }
        }
        currencyViewModel.convertedToPosition.observe(viewLifecycleOwner) {
            binding.spConvertedCurrency.setSelection(it)
        }
        currencyViewModel.basePosition.observe(viewLifecycleOwner) {
            binding.spBaseCurrency.setSelection(it)
        }
    }

    private fun setSpinnerListener() {
        binding.spBaseCurrency.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    currencyViewModel.setBaseCurrency(position)
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

            }
        binding.spConvertedCurrency.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    currencyViewModel.setConvertToCurrency(position)
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

            }
    }

    private fun setTextWatcherListener() {
        binding.edBaseCurrency.doAfterTextChanged {
            currencyViewModel.setBaseCurrencyRate(it.toString())
        }
    }
}
