package com.example.currencyapplication.presentation.currencyDetails.itemviewmodels

import com.example.currencyapplication.R
import com.example.currencyapplication.presentation.currencyDetails.adapter.ItemViewModel
import com.example.currencyapplication.presentation.currencyDetails.viewModel.CurrencyDetailsViewModel

class CurrencyListingViewModel(
    val baseCurrency: String,
    val convertToCurrency: String
) : ItemViewModel {

    override val layoutId: Int = R.layout.item_currecny_listing

    override val viewType: Int = CurrencyDetailsViewModel.LISTING_ITEM

}