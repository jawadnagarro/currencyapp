package com.example.currencyapplication.presentation.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel()