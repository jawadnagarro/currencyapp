package com.example.currencyapplication.presentation.currencyDetails.itemviewmodels

import com.example.currencyapplication.R
import com.example.currencyapplication.presentation.currencyDetails.adapter.ItemViewModel
import com.example.currencyapplication.presentation.currencyDetails.viewModel.CurrencyDetailsViewModel

class HeaderViewModel(val title: String) : ItemViewModel {

    override val layoutId: Int = R.layout.item_date

    override val viewType: Int = CurrencyDetailsViewModel.DATE_ITEM
}