package com.example.currencyapplication.presentation.convertCurrency.viewModel

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.currencyapplication.data.remote.helper.APIResult
import com.example.currencyapplication.domain.usecase.CurrencySymbolUseCase
import com.example.currencyapplication.domain.usecase.LatestRateUseCase
import com.example.currencyapplication.domain.usecase.interactor.UseCase
import com.example.currencyapplication.presentation.base.BaseViewModel
import com.example.currencyapplication.utils.CurrencyUtil
import com.example.currencyapplication.utils.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlin.random.Random

@HiltViewModel
class ConvertCurrencyViewModel @Inject constructor(
    private val currencySymbolUseCase: CurrencySymbolUseCase,
    private val latestRateUseCase: LatestRateUseCase,
) : BaseViewModel() {

    var _currencyList: MutableLiveData<List<String>> = MutableLiveData()
    val currencyList: LiveData<List<String>> get() = _currencyList

    var _currencyRateList: MutableLiveData<Map<String, Double>> = MutableLiveData()

    private var _progressStatus: MutableLiveData<Boolean> = MutableLiveData()
    val progressStatus: LiveData<Boolean> get() = _progressStatus

    private var _apiMessage: MutableLiveData<Event<String>> = MutableLiveData()
    val apiMessage: LiveData<Event<String>> get() = _apiMessage

    var _base: MutableLiveData<String> = MutableLiveData()
    var _convertedTo: MutableLiveData<String> = MutableLiveData()

    //Holds the base spinner item position
    var _basePosition: MutableLiveData<Int> = MutableLiveData(0)
    val basePosition: LiveData<Int> get() = _basePosition

    //Holds the convert to currency spinner item position
    var _convertedToPosition: MutableLiveData<Int> = MutableLiveData()
    val convertedToPosition: LiveData<Int> get() = _convertedToPosition

    //Holds the base currency value
    private var _baseCurrency: MutableLiveData<Double> = MutableLiveData(1.00)

    //Holds the convert to currency value
    private var _convertedToCurrency: MutableLiveData<Double> = MutableLiveData(0.0)
    val convertedToCurrency: LiveData<Double> get() = _convertedToCurrency

    init {
        loadCurrencySymbols()
    }

    /**
     * load all available currency symbols
     */
    private fun loadCurrencySymbols() {
        currencySymbolUseCase(UseCase.None(), viewModelScope) {
            when (it.status) {
                APIResult.Status.SUCCESS -> {
                    it.data?.let { currencyListData ->
                        _currencyList.value = currencyListData
                        setRandomConvertToSymbol(currencyListData)
                    }
                }
                APIResult.Status.ERROR -> {
                    _apiMessage.value = Event(it.message!!)
                }
                APIResult.Status.LOADING -> _progressStatus.value = true
            }
        }
    }

    private fun setRandomConvertToSymbol(currencySymbolList: List<String>?) {
        Handler(Looper.myLooper()!!).postDelayed({
            currencySymbolList?.let {
                val randomIndex = Random.nextInt(it.size)
                _convertedTo.value = it[randomIndex]
                _convertedToPosition.value = randomIndex
            }
            loadLatestCurrencyRate()
        }, 500)
    }

    /**
     * Gets the latest currency rate
     * */
    private fun loadLatestCurrencyRate() {
        latestRateUseCase(_currencyList.value!!, viewModelScope) {
            when (it.status) {
                APIResult.Status.SUCCESS -> {
                    _progressStatus.value = false
                    _currencyRateList.value = it.data!!
                    getCurrencyRates()
                }
                APIResult.Status.ERROR -> {
                    _progressStatus.value = false
                    _apiMessage.value = Event(it.message!!)
                }
                APIResult.Status.LOADING -> _progressStatus.value = true
            }
        }
    }

    /**
     * Set's the base currency value by item position
     */
    fun setBaseCurrency(position: Int) {
        _currencyList.value?.let {
            if (position < it.size) {
                _base.value = it[position]
                getCurrencyRates()
            }
        }
    }

    /**
     * Set's the convert to currency value by item position
     */
    fun setConvertToCurrency(position: Int) {
        _currencyList.value?.let {
            if (position < it.size) {
                _convertedTo.value = it[position]
                getCurrencyRates()
            }
        }
    }

    /**
     * Set's the base currency rate
     */
    fun setBaseCurrencyRate(baseValue: String) {
        if (baseValue.isNotEmpty()) {
            _baseCurrency.value = baseValue.toDouble()
            getCurrencyRates()
        }
    }

    /**
     * Changing the currency value between two spinner
     */
    fun swapCurrencyValue() {
        _currencyList.value?.let {
            val initBasePosition = it.indexOf(_base.value)
            val initConvertedToPosition = it.indexOf(_convertedTo.value)
            _basePosition.value = initConvertedToPosition
            _convertedToPosition.value = initBasePosition
        }
    }

    /**
     * Converts the currency rate
     */
    private fun getCurrencyRates() {
        if (_base.value != null && _convertedTo.value != null) {
            val baseValue = _base.value!!
            val convertedToValue = _convertedTo.value!!
            _currencyRateList.value?.let {
                val currencyValue =
                    if (baseValue != convertedToValue) {
                        CurrencyUtil().convertCurrencyRate(it[baseValue]!!, it[convertedToValue]!!)
                    } else 1.0
                _convertedToCurrency.value = currencyValue * _baseCurrency.value!!
            }
        }
    }
}