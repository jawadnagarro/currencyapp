package com.example.currencyapplication.data.remote.dataSource

import com.example.currencyapplication.data.remote.helper.BaseDataSource
import com.example.currencyapplication.data.remote.helper.NetworkHandler
import com.example.currencyapplication.data.remote.service.APIServices
import javax.inject.Inject

class CurrencyDataSource @Inject constructor(
    networkHandler: NetworkHandler,
    private val apiServices: APIServices
) : BaseDataSource(networkHandler) {

    suspend fun getCurrencySymbolList(accessKey: String) = getResult {
        apiServices.getAllCurrencySymbol(accessKey)
    }

    suspend fun getLatestCurrencyRate(accessKey: String, base: String, symbols: String) =
        getResult {
            apiServices.getLatestCurrencyRate(accessKey, base, symbols)
        }

    suspend fun getHistoricalCurrencyRate(
        date: String,
        accessKey: String,
        base: String,
        symbols: String
    ) = getResult {
        apiServices.getHistoricalRates(date, accessKey, base, symbols)
    }
}