package com.example.currencyapplication.data.model

import com.example.currencyapplication.data.remote.helper.ErrorModel

data class LatestCurrency(
    val base: String,
    val date: String,
    val rates: Map<String, Double>,
    val success: Boolean,
    val timestamp: Int,
    val error: ErrorModel
)