package com.example.currencyapplication.data.model

import com.example.currencyapplication.data.remote.helper.ErrorModel

data class CurrencySymbol(
    val symbols: Map<String, String>,
    val success: Boolean,
    val error: ErrorModel
)