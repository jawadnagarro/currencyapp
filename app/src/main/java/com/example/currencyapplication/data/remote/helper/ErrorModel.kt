package com.example.currencyapplication.data.remote.helper


data class ErrorModel(
    val code: Int,
    val type: String,
    val info: String?
)
