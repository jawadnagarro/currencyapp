package com.example.currencyapplication.data.remote.service

import com.example.currencyapplication.data.model.CurrencySymbol
import com.example.currencyapplication.data.model.HistoricalRates
import com.example.currencyapplication.data.model.LatestCurrency
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface APIServices {
    companion object {

        private const val ACCESS_KEY = "access_key"
        private const val BASE = "base"
        private const val SYMBOLS = "symbols"
        private const val DATE = "date"

        private const val SYMBOL_LIST = "api/symbols?"
        private const val LATEST = "api/latest?"
        private const val HISTORICAL_RATE = "api/{$DATE}?"
    }

    @GET(SYMBOL_LIST)
    suspend fun getAllCurrencySymbol(
        @Query(ACCESS_KEY) accessKey: String
    ): Response<CurrencySymbol>

    @GET(LATEST)
    suspend fun getLatestCurrencyRate(
        @Query(ACCESS_KEY) accessKey: String,
        @Query(BASE) base: String,
        @Query(SYMBOLS) symbols: String
    ): Response<LatestCurrency>

    @GET(HISTORICAL_RATE)
    suspend fun getHistoricalRates(
        @Path(DATE) date: String,
        @Query(ACCESS_KEY) accessKey: String,
        @Query(BASE) base: String,
        @Query(SYMBOLS) symbols: String
    ): Response<HistoricalRates>
}