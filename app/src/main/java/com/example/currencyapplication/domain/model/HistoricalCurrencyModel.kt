package com.example.currencyapplication.domain.model

data class HistoricalCurrencyModel(
    val base: String,
    val date: String,
    val rates: Map<String, Double>
)