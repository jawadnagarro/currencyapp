package com.example.currencyapplication.domain.usecase

import com.example.currencyapplication.data.remote.helper.APIResult
import com.example.currencyapplication.domain.repository.CurrencyRepository
import com.example.currencyapplication.domain.usecase.interactor.UseCase
import javax.inject.Inject

class LatestRateUseCase @Inject constructor(private val repository: CurrencyRepository) :
    UseCase<Map<String, Double>, List<String>>() {

    override suspend fun run(params: List<String>): APIResult<Map<String, Double>> =
        repository.getLatestCurrencyRate(params)

}