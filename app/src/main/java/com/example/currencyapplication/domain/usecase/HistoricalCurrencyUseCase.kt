package com.example.currencyapplication.domain.usecase

import com.example.currencyapplication.data.remote.helper.APIResult
import com.example.currencyapplication.domain.model.HistoricalCurrencyModel
import com.example.currencyapplication.domain.repository.CurrencyRepository
import com.example.currencyapplication.utils.DateUtil
import kotlinx.coroutines.*
import javax.inject.Inject

class HistoricalCurrencyUseCase @Inject constructor(private val repository: CurrencyRepository) {

    /**
     * Gets the historical currency data by day's
     */
    fun getHistoricalCurrencyData(
        availableCurrencyList: List<String>,
        scope: CoroutineScope,
        onResult: (APIResult<Map<String, HistoricalCurrencyModel>>) -> Unit
    ) {
        onResult(APIResult.loading())
        scope.launch(Dispatchers.Main) {
            val historicalCurrencyData: MutableMap<String, HistoricalCurrencyModel> = mutableMapOf()
            val apiResponseList: ArrayList<APIResult<HistoricalCurrencyModel>> = arrayListOf()
            val dayCount = 6
            for (dayOf in 0..dayCount) {
                val dateValue = dayOf * -1
                val dateResult = DateUtil().getPreviousDate(dateValue)
                val deferredResponseResult = async {
                    repository.getHistoricalCurrencyRate(
                        dateResult,
                        availableCurrencyList
                    )
                }
                apiResponseList.add(deferredResponseResult.await())
            }

            apiResponseList.forEach { apiResult ->
                try {
                    if (apiResult.status == APIResult.Status.SUCCESS) {
                        apiResult.data?.let {
                            historicalCurrencyData[it.date] = it
                        }
                    }
                } catch (e: Exception) {
                    println(e.message)
                }
            }
            onResult(APIResult.success(historicalCurrencyData))
        }
    }
}