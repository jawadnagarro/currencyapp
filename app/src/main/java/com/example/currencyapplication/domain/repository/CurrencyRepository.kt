package com.example.currencyapplication.domain.repository

import com.example.currencyapplication.BuildConfig
import com.example.currencyapplication.data.remote.dataSource.CurrencyDataSource
import com.example.currencyapplication.data.remote.helper.APIResult
import com.example.currencyapplication.domain.dto.HistoricalCurrencyRateDTO
import com.example.currencyapplication.domain.model.HistoricalCurrencyModel
import java.lang.Exception
import javax.inject.Inject

class CurrencyRepository @Inject constructor(private val currencyDataSource: CurrencyDataSource) {

    companion object {
        private const val baseCurrency = "EUR"
    }

    /**
     * Currency symbol list API response handled in this function
     * returns Currency symbol list from the server
     */
    suspend fun getCurrencySymbol(): APIResult<List<String>> {
        return try {
            val response = currencyDataSource.getCurrencySymbolList(BuildConfig.ACCESS_KEY)
            if (response.status == APIResult.Status.SUCCESS) {
                response.data?.let { currencyData ->
                    if (currencyData.success) {
                        val currencySymbolList: List<String> =
                            ArrayList<String>(currencyData.symbols.keys)
                        if (currencySymbolList.isNotEmpty())
                            APIResult.success(currencySymbolList)
                        else
                            APIResult.error("No currency symbol found")
                    } else {
                        currencyData.error.info?.let { errorInfo ->
                            APIResult.error(errorInfo)
                        } ?: run {
                            APIResult.error(currencyData.error.type)
                        }
                    }
                } ?: run {
                    APIResult.error("No data found")
                }
            } else {
                APIResult.error(response.message!!)
            }
        } catch (ex: Exception) {
            APIResult.error("Sorry, something went wrong")
        }
    }

    /**
     * The latest currency rate API response handled in this function
     * returns latest currency rate from the server
     */
    suspend fun getLatestCurrencyRate(availableCurrencyList: List<String>): APIResult<Map<String, Double>> {
        return try {
            val response =
                currencyDataSource.getLatestCurrencyRate(
                    BuildConfig.ACCESS_KEY,
                    baseCurrency,
                    availableCurrencyList.joinToString()
                )
            if (response.status == APIResult.Status.SUCCESS) {
                response.data?.let { currencyData ->
                    if (currencyData.success) {
                        if (currencyData.rates.isNotEmpty())
                            APIResult.success(currencyData.rates)
                        else
                            APIResult.error("No currency symbol found")
                    } else {
                        currencyData.error.info?.let { errorInfo ->
                            APIResult.error(errorInfo)
                        } ?: run {
                            APIResult.error(currencyData.error.type)
                        }
                    }
                } ?: run {
                    APIResult.error("No data found")
                }
            } else {
                APIResult.error(response.message!!)
            }
        } catch (ex: Exception) {
            APIResult.error("Sorry, something went wrong")
        }
    }

    /**
     * The historical rate API response handled in this function
     * returns Historical rate from the server
     */
    suspend fun getHistoricalCurrencyRate(
        date: String,
        availableCurrencyList: List<String>
    ): APIResult<HistoricalCurrencyModel> {
        return try {
            val response =
                currencyDataSource.getHistoricalCurrencyRate(
                    date,
                    BuildConfig.ACCESS_KEY,
                    baseCurrency,
                    availableCurrencyList.joinToString()
                )
            if (response.status == APIResult.Status.SUCCESS) {
                response.data?.let { currencyData ->
                    if (currencyData.success) {
                        if (currencyData.rates.isNotEmpty()) {
                            val historicalRates =
                                HistoricalCurrencyRateDTO()
                                    .transferHistoricalCurrencyRateToHistoricalCurrencyModel(
                                        currencyData
                                    )
                            APIResult.success(historicalRates)
                        } else
                            APIResult.error("No currency symbol found")
                    } else {
                        currencyData.error.info?.let { errorInfo ->
                            APIResult.error(errorInfo)
                        } ?: run {
                            APIResult.error(currencyData.error.type)
                        }
                    }
                } ?: run {
                    APIResult.error("No data found")
                }
            } else {
                APIResult.error(response.message!!)
            }
        } catch (ex: Exception) {
            APIResult.error("Sorry, something went wrong")
        }
    }
}