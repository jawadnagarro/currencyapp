package com.example.currencyapplication.domain.dto

import com.example.currencyapplication.data.model.HistoricalRates
import com.example.currencyapplication.domain.model.HistoricalCurrencyModel

class HistoricalCurrencyRateDTO {

    fun transferHistoricalCurrencyRateToHistoricalCurrencyModel(historicalRates: HistoricalRates) =
        HistoricalCurrencyModel(
            historicalRates.base,
            historicalRates.date,
            historicalRates.rates
        )
}