package com.example.currencyapplication.domain.usecase

import com.example.currencyapplication.data.remote.helper.APIResult
import com.example.currencyapplication.domain.repository.CurrencyRepository
import com.example.currencyapplication.domain.usecase.interactor.UseCase
import javax.inject.Inject

class CurrencySymbolUseCase @Inject constructor(private val repository: CurrencyRepository) :
    UseCase<List<String>, UseCase.None>() {

    override suspend fun run(params: None): APIResult<List<String>> = repository.getCurrencySymbol()

}