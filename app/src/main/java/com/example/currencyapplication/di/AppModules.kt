package com.example.currencyapplication.di

import com.example.currencyapplication.BuildConfig
import com.example.currencyapplication.data.remote.dataSource.CurrencyDataSource
import com.example.currencyapplication.data.remote.helper.NetworkHandler
import com.example.currencyapplication.data.remote.service.APIServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [CoreDataModules::class])
@InstallIn(SingletonComponent::class)
object AppModules {

    @Provides
    @Singleton
    fun providesAPIServices(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ) = provideServices(okHttpClient, gsonConverterFactory, APIServices::class.java)

    @Provides
    @Singleton
    fun provideNewsDataSource(networkHandler: NetworkHandler, apiServices: APIServices) =
        CurrencyDataSource(networkHandler, apiServices)

    private fun <T> provideServices(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory, newClass: Class<T>
    ) = createRetrofit(okHttpClient, gsonConverterFactory).create(newClass)

    private fun createRetrofit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ) =
        Retrofit.Builder().baseUrl(BuildConfig.API_BASE_URL).client(okHttpClient)
            .addConverterFactory(gsonConverterFactory).build()
}