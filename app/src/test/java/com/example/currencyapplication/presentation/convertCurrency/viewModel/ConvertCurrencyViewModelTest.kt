package com.example.currencyapplication.presentation.convertCurrency.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.currencyapplication.domain.usecase.CurrencySymbolUseCase
import com.example.currencyapplication.domain.usecase.LatestRateUseCase
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.*


@RunWith(JUnit4::class)
class ConvertCurrencyViewModelTest {
    @get:Rule
    val instantTaskRule = InstantTaskExecutorRule()

    private val currencySymbolUseCase = mock(CurrencySymbolUseCase::class.java)
    private val latestRateUseCase = mock(LatestRateUseCase::class.java)

    lateinit var convertCurrencyViewModel: ConvertCurrencyViewModel

    @Before
    fun setup() {
        convertCurrencyViewModel =
            ConvertCurrencyViewModel(currencySymbolUseCase, latestRateUseCase)
    }

    @Test
    fun `verify when the base spinner index position value is grater than the size of currencyList _base value must be null`() {
        val spinnerIndex = 12
        convertCurrencyViewModel._currencyList.value = localTestCurrencyList
        convertCurrencyViewModel.setBaseCurrency(spinnerIndex)
        assertThat(convertCurrencyViewModel._base.value).isNull()
    }

    @Test
    fun `verify that when the base spinner index is passed to setBaseCurrency method it reflect on _base livedata`() {
        val spinnerIndex = 2
        convertCurrencyViewModel._currencyList.value = localTestCurrencyList
        convertCurrencyViewModel.setBaseCurrency(spinnerIndex)
        assertThat(convertCurrencyViewModel._base.value).isNotNull()
    }

    @Test
    fun `verify when the base currency symbol is different for the given index position`() {
        val spinnerIndex = 2
        val currencySymbol = "USA"
        convertCurrencyViewModel._currencyList.value = localTestCurrencyList
        convertCurrencyViewModel.setBaseCurrency(spinnerIndex)
        assertThat(convertCurrencyViewModel._base.value).isNotEqualTo(currencySymbol)
    }

    @Test
    fun `verify when the base currency symbol is same for the given index position`() {
        val spinnerIndex = 2
        val currencySymbol = localTestCurrencyList[spinnerIndex]
        convertCurrencyViewModel._currencyList.value = localTestCurrencyList
        convertCurrencyViewModel.setBaseCurrency(spinnerIndex)
        assertThat(convertCurrencyViewModel._base.value).isEqualTo(currencySymbol)
    }

    @Test
    fun `verify when the convertTo spinner index position value is grater than the size of currencyList _convertedTo value must be null`() {
        val spinnerIndex = 30
        convertCurrencyViewModel._currencyList.value = localTestCurrencyList
        convertCurrencyViewModel.setConvertToCurrency(spinnerIndex)
        assertThat(convertCurrencyViewModel._convertedTo.value).isNull()
    }

    @Test
    fun `verify that when the convertTo is passed to setConvertToCurrency method it reflect on _convertedTo livedata`() {
        val spinnerIndex = 2
        convertCurrencyViewModel._currencyList.value = localTestCurrencyList
        convertCurrencyViewModel.setConvertToCurrency(spinnerIndex)
        assertThat(convertCurrencyViewModel._convertedTo.value).isNotNull()
    }

    @Test
    fun `should passed when swap spinner position between base and convertTo method is invoked`() {
        convertCurrencyViewModel._currencyList.value = localTestCurrencyList
        convertCurrencyViewModel._base.value = baseCurrency
        convertCurrencyViewModel._convertedTo.value = convertToCurrency

        convertCurrencyViewModel.swapCurrencyValue()
        assertThat(convertCurrencyViewModel._basePosition.value).isEqualTo(convertToCurrencyPosition)
        assertThat(convertCurrencyViewModel._convertedToPosition.value).isEqualTo(baseCurrencyPosition)
    }

    companion object {
        private const val baseCurrencyPosition = 0
        private const val convertToCurrencyPosition = 3
        private const val baseCurrency = "AED"
        private const val convertToCurrency = "USD"

        private val localTestCurrencyList = listOf(
            "AED",
            "CAD",
            "GBP",
            "USD",
            "SAR"
        )
    }
}