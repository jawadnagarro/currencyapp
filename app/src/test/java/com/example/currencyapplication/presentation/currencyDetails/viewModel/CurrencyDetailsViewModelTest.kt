package com.example.currencyapplication.presentation.currencyDetails.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import com.example.currencyapplication.domain.usecase.HistoricalCurrencyUseCase
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.*

@RunWith(JUnit4::class)
class CurrencyDetailsViewModelTest {
    @get:Rule
    val instantTaskRule = InstantTaskExecutorRule()


    private val historicalCurrencyUseCase = mock(HistoricalCurrencyUseCase::class.java)
    lateinit var currencyDetailsViewModel: CurrencyDetailsViewModel

    @Mock
    private lateinit var isLoadingLiveData: LiveData<Boolean>

    @Before
    fun setup() {
        currencyDetailsViewModel = CurrencyDetailsViewModel(historicalCurrencyUseCase)

        currencyDetailsViewModel.currencyList = localTestCurrencyList
        currencyDetailsViewModel.baseCurrency = baseCurrency
        currencyDetailsViewModel.convertToCurrency = convertToCurrency
        isLoadingLiveData = currencyDetailsViewModel.progressStatus
    }

    @Test
    fun `verify if all the livedata values are null value`() {
        assertThat(currencyDetailsViewModel.progressStatus.value).isNull()
        assertThat(currencyDetailsViewModel.apiMessage.value).isNull()
        assertThat(currencyDetailsViewModel.otherCurrencyData.value).isNull()
        assertThat(currencyDetailsViewModel.historicalCurrencyData.value).isEmpty()
    }

    @Test
    fun `should return empty string when localCurrencyList is passed as empty list`() {
        val data = currencyDetailsViewModel.getOtherCurrencyList(
            mapTestCurrencyRateList,
            emptyList()
        )
        assertThat(data).isEmpty()
    }

    @Test
    fun `should return empty string when mapTestCurrencyRateList is passed as empty map`() {
        val data = currencyDetailsViewModel.getOtherCurrencyList(
            emptyMap(),
            localTestCurrencyList
        )
        assertThat(data).isEmpty()
    }

    @Test
    fun `should return other currency string when getOtherCurrencyList called`() {
        val data = currencyDetailsViewModel.getOtherCurrencyList(
            mapTestCurrencyRateList,
            localTestCurrencyList
        )
        assertThat(data).isNotEmpty()
    }

    companion object {
        private val baseCurrency = "AED"
        private val convertToCurrency = "USD"
        private val localTestCurrencyList = listOf(
            "AED",
            "CAD",
            "GBP",
            "USD",
            "SAR"
        )
        private val mapTestCurrencyRateList =
            mapOf("CAD" to 1.0, "AED" to 12.25, "USD" to 0.12, "GBP" to 65.3, "SAR" to 21.36)
    }
}