package com.example.currencyapplication.data.remote.services

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.currencyapplication.data.remote.service.APIServices
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class APIServicesTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: APIServices
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url(""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(APIServices::class.java)
    }

    @Test
    fun `should fail when get currency symbol list API request when getAllCurrencySymbol returns with failure result`() {
        runBlocking {
            enqueueResponse("currencySymbol.json")
            val resultResponse = service.getAllCurrencySymbol(DUMMY_ACCESS_KEY)
            val request = mockWebServer.takeRequest()
            assertThat(resultResponse).isNotNull()
            assertThat(request.path).isNotEqualTo("sample/test")
        }
    }

    @Test
    fun `should pass when get currency symbol list API request when getAllCurrencySymbol() returns with success result`() {
        runBlocking {
            enqueueResponse("currencySymbol.json")
            val resultResponse = service.getAllCurrencySymbol(DUMMY_ACCESS_KEY)
            val request = mockWebServer.takeRequest()
            assertThat(resultResponse).isNotNull()
            assertThat(request.path).isEqualTo(DUMMY_SYMBOL_LIST)
        }
    }

    @Test
    fun `get currency symbol list success returns with success with inappropriate data size`() {
        val currencyListSize = 15
        runBlocking {
            enqueueResponse("currencySymbol.json")
            val resultResponse = service.getAllCurrencySymbol(DUMMY_ACCESS_KEY)
            assertThat(resultResponse.body()!!.symbols.size).isNotEqualTo(currencyListSize)
        }
    }

    @Test
    fun `get currency symbol list success returns with success with appropriate data size`() {
        val currencySymbolListSize = 156
        runBlocking {
            enqueueResponse("currencySymbol.json")
            val resultResponse = service.getAllCurrencySymbol(DUMMY_ACCESS_KEY).body()
            val currencySymbolList = resultResponse!!
            assertThat(currencySymbolList.symbols.size).isEqualTo(currencySymbolListSize)
        }
    }

    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader!!
            .getResourceAsStream("api-response/$fileName")
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(
            mockResponse.setBody(
                source.readString(Charsets.UTF_8)
            )
        )
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    companion object {
        private const val DUMMY_ACCESS_KEY = "access_key"
        private const val DATE = "date"

        private const val DUMMY_SYMBOL_LIST = "/api/symbols?&access_key=$DUMMY_ACCESS_KEY"
    }
}