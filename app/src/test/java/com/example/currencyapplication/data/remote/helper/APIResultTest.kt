package com.example.currencyapplication.data.remote.helper

import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class APIResultTest {
    @Test
    fun `APIResult should return same instance for success type`() {
        val result = APIResult.success(DUMMY_DATA)
        assertThat(result).isInstanceOf(APIResult::class.java)
        assertThat(result.status).isEqualTo(APIResult.Status.SUCCESS)
        assertThat(result.data).isEqualTo(DUMMY_DATA)
    }

    @Test
    fun `APIResult should return wrong type for success type`() {
        val result = APIResult.success(DUMMY_DATA)
        assertThat(result.status).isNotEqualTo(APIResult.Status.ERROR)
    }

    @Test
    fun `APIResult should return same instance for error type with same error message`() {
        val result = APIResult.error(data = null, message = DUMMY_DATA_ERROR)
        assertThat(result).isInstanceOf(APIResult::class.java)
        assertThat(result.status).isEqualTo(APIResult.Status.ERROR)
        assertThat(result.message).isEqualTo(DUMMY_DATA_ERROR)
    }

    @Test
    fun `APIResult should return same status for error type with different error message`() {
        val result = APIResult.error(data = null, message = DUMMY_DATA_ERROR)
        assertThat(result.status).isEqualTo(APIResult.Status.ERROR)
    }

    companion object {
        private const val DUMMY_DATA = "Dummy data"
        private const val DUMMY_DATA_ERROR = "Dummy data error"
    }
}