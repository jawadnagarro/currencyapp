package com.example.currencyapplication.domain.interactor

import com.example.currencyapplication.data.remote.helper.APIResult
import com.example.currencyapplication.domain.usecase.interactor.UseCase
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class UseCaseTest {
    private lateinit var useCase: TestUseCase

    @Before
    fun testUseCase() {
        useCase = TestUseCase()
    }

    @Test
    fun `use case should return 'APIResult' of use case type with success status`() {
        val params = MyParams(TYPE_PARAM)
        val result = runBlocking { useCase.run(params) }
        assertThat(result).isEqualTo(APIResult.success(MyType(TYPE_TEST)))
    }

    data class MyType(val name: String)
    data class MyParams(val name: String)

    private inner class TestUseCase : UseCase<MyType, MyParams>() {
        override suspend fun run(params: MyParams) = APIResult.success(MyType(TYPE_TEST))
    }

    companion object {
        private const val TYPE_TEST = "Test"
        private const val TYPE_PARAM = "ParamTest"
    }
}