package com.example.currencyapplication.utils

import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class CurrencyUtilTest {
    private val currencyUtil = CurrencyUtil()

    private val baseCurrency = 12.0
    private val convertToCurrency = 11.0
    private val currencyResult = 0.9166666666666666

    @Test
    fun `should fail when convertCurrencyRate method is called returns wrong result`() {
        val currencyWrongResult = 120.98
        val mockCurrencyUtil = mock(CurrencyUtil::class.java)
        whenever(
            mockCurrencyUtil.convertCurrencyRate(
                baseCurrency,
                convertToCurrency
            )
        ) doReturn currencyWrongResult
        val result = mockCurrencyUtil.convertCurrencyRate(baseCurrency, convertToCurrency)
        assertThat(result).isNotEqualTo(currencyResult)
    }

    @Test
    fun `should success when convertCurrencyRate method is called returns proper conversion result`() {
        val result = currencyUtil.convertCurrencyRate(baseCurrency, convertToCurrency)
        assertThat(result).isEqualTo(currencyResult)
    }

    @Test
    fun `should success when convertToCurrency 0 convertCurrencyRate method is called returns 0 result`() {
        val result = currencyUtil.convertCurrencyRate(baseCurrency, 0.0)
        assertThat(result).isEqualTo(0.0)
    }
}