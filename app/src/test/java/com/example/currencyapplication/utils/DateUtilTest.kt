package com.example.currencyapplication.utils

import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.whenever
import java.text.SimpleDateFormat
import java.util.*

@RunWith(JUnit4::class)
class DateUtilTest {

    private val dateUtil = mock(DateUtil::class.java)
    private val currentDate = SimpleDateFormat(DEFAULT_FORMAT_DATE).format(
        Date()
    )

    @Test
    fun `should fail when current date method is called returns wrong result`() {
        val dateValue = "2022-03-12"
        whenever(dateUtil.getCurrentDate()) doReturn dateValue
        val result = dateUtil.getCurrentDate()
        assertThat(result).isNotEqualTo(currentDate)
    }

    @Test
    fun `should fail when current date method is called returns null result`() {
        whenever(dateUtil.getCurrentDate()) doReturn ""
        val result = dateUtil.getCurrentDate()
        assertThat(result).isNull()
        assertThat(result).isNotEqualTo(currentDate)
    }

    @Test
    fun `should fail when getPreviousDate() method is called by passing -1, the method is called and returns today's date`() {
        val previousDate = getPreviousDate()
        whenever(dateUtil.getPreviousDate(-1)) doReturn currentDate
        val result = dateUtil.getPreviousDate(-1)
        assertThat(result).isNotEqualTo(previousDate)
    }

    @Test
    fun `should success when getPreviousDate() method is called by passing -1, the method is called and returns previous date`() {
        val previousDate = getPreviousDate()
        whenever(dateUtil.getPreviousDate(-1)) doReturn previousDate

        val result = dateUtil.getPreviousDate(-1)
        assertThat(result).isEqualTo(previousDate)
    }

    @Test
    fun `should success when getPreviousDate() method is called by passing 0, the method is called and returns today's date`() {
        whenever(dateUtil.getPreviousDate(0)) doReturn currentDate
        val result = dateUtil.getPreviousDate(0)
        assertThat(result).isEqualTo(currentDate)
    }

    private fun getPreviousDate(): String {
        val dateFormat = SimpleDateFormat(DateUtil.DEFAULT_FORMAT_DATE, Locale.getDefault())
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.add(Calendar.DATE, -1)
        return dateFormat.format(calendar.time)
    }

    companion object {
        const val DEFAULT_FORMAT_DATE = "yyyy-MM-dd"
    }
}